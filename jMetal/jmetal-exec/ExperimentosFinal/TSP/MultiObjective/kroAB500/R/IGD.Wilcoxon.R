write("", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex",append=FALSE)
resultDirectory<-"ExperimentosFinal/TSP/MultiObjective/kroAB500/data"
latexHeader <- function() {
  write("\\documentclass{article}", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write("\\title{StandardStudy}", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write("\\usepackage{amssymb}", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write("\\author{A.J.Nebro}", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write("\\begin{document}", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write("\\maketitle", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write("\\section{Tables}", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write("\\", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
}

latexTableHeader <- function(problem, tabularString, latexTableFirstLine) {
  write("\\begin{table}", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write("\\caption{", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write(problem, "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write(".IGD.}", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)

  write("\\label{Table:", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write(problem, "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write(".IGD.}", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)

  write("\\centering", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write("\\begin{scriptsize}", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write("\\begin{tabular}{", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write(tabularString, "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write("}", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write(latexTableFirstLine, "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write("\\hline ", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
}

printTableLine <- function(indicator, algorithm1, algorithm2, i, j, problem) { 
  file1<-paste(resultDirectory, algorithm1, sep="/")
  file1<-paste(file1, problem, sep="/")
  file1<-paste(file1, indicator, sep="/")
  data1<-scan(file1)
  file2<-paste(resultDirectory, algorithm2, sep="/")
  file2<-paste(file2, problem, sep="/")
  file2<-paste(file2, indicator, sep="/")
  data2<-scan(file2)
  if (i == j) {
    write("-- ", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  }
  else if (i < j) {
    if (is.finite(wilcox.test(data1, data2)$p.value) & wilcox.test(data1, data2)$p.value <= 0.05) {
      if (median(data1) <= median(data2)) {
        write("$\\blacktriangle$", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
}
      else {
        write("$\\triangledown$", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
}
    }
    else {
      write("--", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
    }
  }
  else {
    write(" ", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  }
}

latexTableTail <- function() { 
  write("\\hline", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write("\\end{tabular}", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write("\\end{scriptsize}", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
  write("\\end{table}", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
}

latexTail <- function() { 
  write("\\end{document}", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
}

### START OF SCRIPT 
# Constants
problemList <-c("kroAB500") 
algorithmList <-c("NSGAII", "MOEAD", "MOCELL", "PAES") 
tabularString <-c("lccc") 
latexTableFirstLine <-c("\\hline  & MOEAD & MOCELL & PAES\\\\ ") 
indicator<-"IGD"

 # Step 1.  Writes the latex header
latexHeader()
tabularString <-c("| l | p{0.15cm } | p{0.15cm } | p{0.15cm } | ") 

latexTableFirstLine <-c("\\hline \\multicolumn{1}{|c|}{} & \\multicolumn{1}{c|}{MOEAD} & \\multicolumn{1}{c|}{MOCELL} & \\multicolumn{1}{c|}{PAES} \\\\") 

# Step 3. Problem loop 
latexTableHeader("kroAB500 ", tabularString, latexTableFirstLine)

indx = 0
for (i in algorithmList) {
  if (i != "PAES") {
    write(i , "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
    write(" & ", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)

    jndx = 0
    for (j in algorithmList) {
      for (problem in problemList) {
        if (jndx != 0) {
          if (i != j) {
            printTableLine(indicator, i, j, indx, jndx, problem)
          }
          else {
            write("  ", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
          } 
          if (problem == "kroAB500") {
            if (j == "PAES") {
              write(" \\\\ ", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
            } 
            else {
              write(" & ", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
            }
          }
     else {
    write("&", "ExperimentosFinal/TSP/MultiObjective/kroAB500/R/IGD.Wilcoxon.tex", append=TRUE)
     }
        }
      }
      jndx = jndx + 1
}
    indx = indx + 1
  }
} # for algorithm

  latexTableTail()

#Step 3. Writes the end of latex file 
latexTail()

