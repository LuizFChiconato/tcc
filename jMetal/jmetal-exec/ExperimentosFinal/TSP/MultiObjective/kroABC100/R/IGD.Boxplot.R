postscript("IGD.Boxplot.eps", horizontal=FALSE, onefile=FALSE, height=8, width=12, pointsize=10)
resultDirectory<-"../data"
qIndicator <- function(indicator, problem)
{
fileNSGAII<-paste(resultDirectory, "NSGAII", sep="/")
fileNSGAII<-paste(fileNSGAII, indicator, sep="/")
NSGAII<-scan(fileNSGAII)

fileMOEAD<-paste(resultDirectory, "MOEAD", sep="/")
fileMOEAD<-paste(fileMOEAD, indicator, sep="/")
MOEAD<-scan(fileMOEAD)

fileMOCELL<-paste(resultDirectory, "MOCELL", sep="/")
fileMOCELL<-paste(fileMOCELL, indicator, sep="/")
MOCELL<-scan(fileMOCELL)

filePAES<-paste(resultDirectory, "PAES", sep="/")
filePAES<-paste(filePAES, indicator, sep="/")
PAES<-scan(filePAES)

algs<-c("NSGAII","MOEAD","MOCELL","PAES")
boxplot(NSGAII,MOEAD,MOCELL,PAES,names=algs, notch = TRUE)
titulo <-paste(indicator, problem, sep=":")
title(main=titulo)
}
par(mfrow=c(3,3))
indicator<-"IGD"
qIndicator(indicator, "kroABC100_4")
