import sys
import csv
import operator
import scipy.stats as ss

filesToRead = [
    'kroAB100',
    'kroAB150',
    'kroAB200',
    'kroAB300',
    'kroAB400',
    'kroAB500',
    'kroAB750',
    'kroAB1000',
    'euclidAB100',
    'euclidAB300',
    'euclidAB500',
    'clusAB100',
    'clusAB300',
    'clusAB500',
    'kroABC100_4',
    'kroABD100',
    'kroABE100',
    'kroACD100',
    'kroACE100',
    'kroADE100',
    'euclABC100',
    'euclDEF100',
    'euclABC300',
    'euclDEF300',
    'euclABC500',
    'euclDEF500',
]

outputStrings = []

for fileToRead in filesToRead:
    reader = csv.reader(open(fileToRead + "/runTimeSummary.tsv"), delimiter="\t")

    sortedlist = sorted(reader, key=operator.itemgetter(0), reverse=True)

    totalPAES = 0
    for i in range(0, 20):
        row = sortedlist[i]
        totalPAES += int(row[6])
    avgPAES = totalPAES/20/1000

    totalNSGAII = 0
    for i in range(20, 40):
        row = sortedlist[i]
        totalNSGAII += int(row[6])
    avgNSGAII = totalNSGAII/20/1000

    totalMOEAD = 0
    for i in range(40, 60):
        row = sortedlist[i]
        totalMOEAD += int(row[6])
    avgMOEAD = totalMOEAD/20/1000

    totalMOCELL = 0
    for i in range(60, 80):
        row = sortedlist[i]
        totalMOCELL += int(row[6])
    avgMOCELL = totalMOCELL/20/1000

    rank = ss.rankdata([avgNSGAII, avgMOEAD, avgMOCELL, avgPAES])
    
    outputString = fileToRead + ' & '
    if (rank[0] == 1):
        outputString += '\\cellcolor{gray95}'
    elif (rank[0] == 2):
        outputString += '\\cellcolor{gray25}'
    outputString += '$     ' + "{:.2f}".format(avgNSGAII) + "$ & "

    if (rank[1] == 1):
        outputString += '\\cellcolor{gray95}'
    elif (rank[1] == 2):
        outputString += '\\cellcolor{gray25}'
    outputString += '$     ' + "{:.2f}".format(avgMOEAD) + "$ & "

    if (rank[2] == 1):
        outputString += '\\cellcolor{gray95}'
    elif (rank[2] == 2):
        outputString += '\\cellcolor{gray25}'
    outputString += '$     ' + "{:.2f}".format(avgMOCELL) + "$ & "

    if (rank[3] == 1):
        outputString += '\\cellcolor{gray95}'
    elif (rank[3] == 2):
        outputString += '\\cellcolor{gray25}'
    outputString += '$     ' + "{:.2f}".format(avgPAES) + "$ "
    outputString += '\\\\'
    outputString += '\n \\hline \n'
    outputStrings.append(outputString)

fw = open('completeTable.txt', 'w')
for outputString in outputStrings:
    fw.write(outputString)
fw.close()

# print(outputString)

# with open('clusAB100/sorted.csv', 'w') as f:
#     for row in sortedlist:
#         writer.writerow(row)
#     writer.writerow(['PAES', '=AVERAGE(G1:G20)', '=AVERAGE(G1:G20)'])
#     writer.writerow(['NSGAII', '=AVERAGE(G21:G40)'])
#     writer.writerow(['MOEAD', '=AVERAGE(G41:G60)'])
#     writer.writerow(['MOCELL', '=AVERAGE(G61:G80)'])

# employee_writer.writerow(['Erica Meyers', 'IT', 'March'])
# print()



# for row in sortedlist:
#     print(row)