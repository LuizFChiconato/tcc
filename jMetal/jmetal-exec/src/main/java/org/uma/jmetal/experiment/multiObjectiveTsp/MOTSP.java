package org.uma.jmetal.experiment.multiObjectiveTsp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.algorithm.multiobjective.mocell.MOCellBuilder;
import org.uma.jmetal.algorithm.multiobjective.moead.MOEADBuilder2;
import org.uma.jmetal.algorithm.multiobjective.nsgaii.NSGAIIBuilder;
import org.uma.jmetal.algorithm.multiobjective.omopso.OMOPSOBuilder;
import org.uma.jmetal.algorithm.multiobjective.paes.PAESBuilder;
import org.uma.jmetal.algorithm.multiobjective.smsemoa.SMSEMOABuilder;
import org.uma.jmetal.operator.impl.crossover.PMXCrossover;
import org.uma.jmetal.operator.impl.mutation.NonUniformMutation;
import org.uma.jmetal.operator.impl.mutation.PermutationSwapMutation;
import org.uma.jmetal.operator.impl.mutation.UniformMutation;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.problem.multiobjective.tsp.TSPMultiobjective;
import org.uma.jmetal.problem.multiobjective.tsp.TSPTriobjective;
import org.uma.jmetal.qualityindicator.impl.Epsilon;
import org.uma.jmetal.qualityindicator.impl.GenerationalDistance;
import org.uma.jmetal.qualityindicator.impl.InvertedGenerationalDistance;
import org.uma.jmetal.qualityindicator.impl.InvertedGenerationalDistancePlus;
import org.uma.jmetal.qualityindicator.impl.Spread;
import org.uma.jmetal.qualityindicator.impl.hypervolume.PISAHypervolume;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.solution.PermutationSolution;
import org.uma.jmetal.util.archive.impl.CrowdingDistanceArchive;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.util.evaluator.impl.SequentialSolutionListEvaluator;
import org.uma.jmetal.util.experiment.Experiment;
import org.uma.jmetal.util.experiment.ExperimentBuilder;
import org.uma.jmetal.util.experiment.component.ComputeQualityIndicators;
import org.uma.jmetal.util.experiment.component.ExecuteAlgorithms;
import org.uma.jmetal.util.experiment.component.GenerateBoxplotsWithR;
import org.uma.jmetal.util.experiment.component.GenerateFriedmanTestTables;
import org.uma.jmetal.util.experiment.component.GenerateLatexTablesWithStatistics;
import org.uma.jmetal.util.experiment.component.GenerateReferenceParetoFront;
import org.uma.jmetal.util.experiment.component.GenerateWilcoxonTestTablesWithR;
import org.uma.jmetal.util.experiment.util.ExperimentAlgorithm;
import org.uma.jmetal.util.experiment.util.ExperimentProblem;

/**
 * Class to configure and run NSGA-II, MOEAD and SMS-EMOA algorithms. The target
 * problem is multiobjective TSP.
 * 
 * @author Mohammed Mahrach <mmahrach@ull.edu.es> based on Antonio J. Nebro
 *         templates <antonio@lcc.uma.es>
 */

public class MOTSP {

	private static final int INDEPENDENT_RUNS = 20;
	private static final int[] populationSizes = { /*20 ,50,*/100/*,200 */ };
	private static final int[] maxEvaluations = { 10000000 /*, 1000000 , 10000000 ,20000000 */ };

	public static void main(String[] args) throws IOException {
//    if (args.length != 1) {
//      throw new JMetalException("Needed arguments: experimentBaseDirectory");
//    }
//    String experimentBaseDirectory = args[0];
		String experimentBaseDirectory = "ExperimentosFinal/TSP/MultiObjective";
		List<ExperimentProblem<PermutationSolution<Integer>>> problemList = new ArrayList<>();

		String path = "/tspInstances/";
//		String instanceName1 = args[0];
//		String instanceName2 = args[1];

		// Examples
		String instanceName1 = "euclidD100";
		String instanceName2 = "euclidE100";
		String fullName = "euclDEF100";
		
		String instanceName3 = "euclidF100";

//		Bi-objective instance
//		problemList.add(new ExperimentProblem<>(
//				new TSPMultiobjective(path + instanceName1 + ".tsp", path + instanceName2 + ".tsp"),
//				fullName));

//		Tri-objective instance
		problemList.add(new ExperimentProblem<>(
				new TSPTriobjective(path + instanceName1 + ".tsp", path + instanceName2 + ".tsp", path + instanceName3 + ".tsp"),
				fullName));

//	    

		List<ExperimentAlgorithm<PermutationSolution<Integer>, List<PermutationSolution<Integer>>>> algorithmList = configureAlgorithmList(
				problemList);

		ExperimentBuilder<PermutationSolution<Integer>, List<PermutationSolution<Integer>>> algorithm = new ExperimentBuilder<PermutationSolution<Integer>, List<PermutationSolution<Integer>>>(
				fullName);
		algorithm.setAlgorithmList(algorithmList);
		algorithm.setProblemList(problemList);
		algorithm.setReferenceFrontDirectory("pareto_fronts/tsp");
		algorithm.setExperimentBaseDirectory(experimentBaseDirectory);
		algorithm.setOutputParetoFrontFileName("FUN");
		algorithm.setOutputParetoSetFileName("VAR");
		algorithm.setIndicatorList(Arrays.asList(new Epsilon<PermutationSolution<Integer>>(),
				new Spread<PermutationSolution<Integer>>(), new GenerationalDistance<PermutationSolution<Integer>>(),
				new PISAHypervolume<PermutationSolution<Integer>>(),
				new InvertedGenerationalDistance<PermutationSolution<Integer>>(),
				new InvertedGenerationalDistancePlus<PermutationSolution<Integer>>()));
		algorithm.setIndependentRuns(INDEPENDENT_RUNS);
		algorithm.setNumberOfCores(6);
		Experiment<PermutationSolution<Integer>, List<PermutationSolution<Integer>>> experiment = algorithm.build();

		new ExecuteAlgorithms<>(experiment).run();
		new GenerateReferenceParetoFront(experiment).run();
		new ComputeQualityIndicators<>(experiment).run();
		new GenerateLatexTablesWithStatistics(experiment).run();
		new GenerateWilcoxonTestTablesWithR<>(experiment).run();
		new GenerateFriedmanTestTables<>(experiment).run();
		new GenerateBoxplotsWithR<>(experiment).setRows(3).setColumns(3).setDisplayNotch().run();
	}

	/**
	 * The algorithm list is composed of pairs {@link Algorithm} + {@link Problem}
	 * which form part of a {@link TaggedAlgorithm}, which is a decorator for class
	 * {@link Algorithm}.
	 *
	 * @param problemList
	 * @return
	 */
	/**
	 * The algorithm list is composed of pairs {@link Algorithm} + {@link Problem}
	 * which form part of a {@link ExperimentAlgorithm}, which is a decorator for
	 * class {@link Algorithm}.
	 */

	static List<ExperimentAlgorithm<PermutationSolution<Integer>, List<PermutationSolution<Integer>>>> configureAlgorithmList(
			List<ExperimentProblem<PermutationSolution<Integer>>> problemList) {

		List<ExperimentAlgorithm<PermutationSolution<Integer>, List<PermutationSolution<Integer>>>> algorithms = new ArrayList<>();

		BinaryTournamentSelection<PermutationSolution<Integer>> selection;
		selection = new BinaryTournamentSelection<PermutationSolution<Integer>>(
				new RankingAndCrowdingDistanceComparator<PermutationSolution<Integer>>());

		for (int i = 0; i < maxEvaluations.length; i++) {
			for (int j = 0; j < populationSizes.length; j++) {
				for (int run = 0; run < INDEPENDENT_RUNS; run++) {

					int maxEval = maxEvaluations[i];
					int popSize = populationSizes[j];

					for (int p = 0; p < problemList.size(); p++) {
						Algorithm<List<PermutationSolution<Integer>>> algorithm = new NSGAIIBuilder<PermutationSolution<Integer>>(
								problemList.get(p).getProblem(), new PMXCrossover(0.9843),
								new PermutationSwapMutation<Integer>(0.0163), 300).setOffspringPopulationSize(300)
										.setMaxEvaluations(maxEval).build();

						algorithms.add(new ExperimentAlgorithm<>(algorithm,
								("NSGAII"), problemList.get(p), run));
					}

					for (int p = 0; p < problemList.size(); p++) {
						Algorithm<List<PermutationSolution<Integer>>> algorithm = new MOEADBuilder2<PermutationSolution<Integer>>(
								problemList.get(p).getProblem(), MOEADBuilder2.Variant.MOEAD, new PMXCrossover(0.9421),
								new PermutationSwapMutation<Integer>(0.0105), 300).setNeighborSize(50)
										.setNeighborhoodSelectionProbability(0.9354).setMaxEvaluations(maxEval)
										.setDataDirectory("MOEAD_Weights").build();

						algorithms.add(new ExperimentAlgorithm<>(algorithm, ("MOEAD"),
								problemList.get(p), run));

					}

					for (int p = 0; p < problemList.size(); p++) {
						Algorithm<List<PermutationSolution<Integer>>> algorithm = new MOCellBuilder<PermutationSolution<Integer>>(
								problemList.get(p).getProblem(),
								new PMXCrossover(0.93),
								new PermutationSwapMutation<Integer>(0.0363)
							).setPopulationSize(289).setMaxEvaluations(maxEval).build();

						algorithms.add(new ExperimentAlgorithm<>(algorithm,
								("MOCELL"), problemList.get(p), run));

					}

					for (int p = 0; p < problemList.size(); p++) {
						Algorithm<List<PermutationSolution<Integer>>> algorithm = new PAESBuilder<PermutationSolution<Integer>>(
								problemList.get(p).getProblem()
							).setArchiveSize(300)
								.setMaxEvaluations(maxEval)
								.setMutationOperator(new PermutationSwapMutation<Integer>(0.0163))
								.setBiSections(4)
								.build();
		
						algorithms.add(new ExperimentAlgorithm<>(algorithm,
								("PAES"), problemList.get(p), run));

					}

				}
			}
		}
		return algorithms;

	}
}
